﻿using System.Windows.Controls;
using ModelsTablesDBLib;
using System.Windows.Controls.WpfPropertyGrid;
using System.Windows;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Windows.Input;
using System.Xml;
using WpfPasswordControlLibrary;
using System.Threading.Tasks;
using System.Threading;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace ControlProperties
{
    /// <summary>
    /// Логика взаимодействия для UserControl1.xaml
    /// </summary>
    public partial class BasicProperties : UserControl
    {
        public BasicProperties()
        {
            InitializeComponent();
            SetResourcelanguage();
            ChangeLanguage();
            InitEditors();
            InitProperties();
            InitEvent();
            InitAccessWindow();
      
            tabControl.SelectedIndex = 1;
        }

        private readonly WindowPass _accessWindowPass = new WindowPass();
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }

        #region Properties

        public GlobalProperties Global
        {
            get { return (GlobalProperties)Resources["globalProperties"]; }
            set
            {
                try
                {
                    ((GlobalProperties)Resources["globalProperties"]).PropertyChanged -= Global_PropertyChanged;
                    if (value == null)
                    {
                        OnUpdateGlobalProperties(this, Global);
                        return;
                    }
                    if ((GlobalProperties)Resources["globalProperties"] == value)
                    {
                        oldGlobal = value.Clone();
                        return;
                    }
                ((GlobalProperties)Resources["globalProperties"]).Update(value);
                    oldGlobal = Global.Clone();
                }
                finally
                {
                    ((GlobalProperties)Resources["globalProperties"]).PropertyChanged += Global_PropertyChanged;
                }
            }
        }

        public LocalProperties Local
        {
            get
            {
                var a = (LocalProperties)Resources["localProperties"];
                return a; }
            set
            {
                if ((LocalProperties)Resources["localProperties"] == value)
                {
                    oldLocal = value.Clone();
                    return;
                }
                ((LocalProperties)Resources["localProperties"]).Update(value);
                oldLocal = Local.Clone();
            }
        }


        private bool isLocalSelected;
        public bool IsLocalSelected
        {
            get => isLocalSelected;
            set
            {
                if (isLocalSelected == value) return;
                isLocalSelected = value;
                OnPropertyChanged();
            }
        }

        private bool isValid;
        public bool IsValid
        {
            get => isValid;
            private set
            {
                if (isValid == value) return;
                isValid = value;
                OnPropertyChanged();
            }
        }

        private string errorMessage;
        public string ErrorMessage
        {
            get => errorMessage;
            set
            {
                if (errorMessage == value) return;
                errorMessage = value;
                OnPropertyChanged();
            }
        }

        private bool _showToolTip;
        public bool ShowToolTip
        {
            get => _showToolTip;
            set
            {
                if (_showToolTip == value) return;
                _showToolTip = value;
                OnPropertyChanged();
            }
        }
        #endregion

        #region Private

        private GlobalProperties oldGlobal;

        private LocalProperties oldLocal;

        #endregion

        #region Events

        public event EventHandler<GlobalProperties> OnUpdateGlobalProperties = (object sender, GlobalProperties arg) => { };

        public event EventHandler<LocalProperties> OnUpdateLocalProperties = (object sender, LocalProperties arg) => { };

        public event EventHandler<Languages> OnLanguageChange = (object sender, Languages arg) => { };

        public event EventHandler<bool> OnSignHeadingAngle = (object sender, bool arg) => { };

        public event EventHandler<bool> OnPasswordChecked = (sender, isCorrect) => { };

        #endregion

        #region Access
        private bool _isAccessChecked = false;

   private void InitAccessWindow()
   {
       _accessWindowPass.OnEnterInvalidPassword += _accessWindowPass_OnEnterInvalidPassword;
       _accessWindowPass.OnEnterValidPassword += _accessWindowPass_OnEnterValidPassword;
       _accessWindowPass.OnClosePasswordBox += _accessWindowPass_OnClosePasswordBox;
       _accessWindowPass.Resources = Resources;
       _accessWindowPass.SetResourceReference(WindowPass.LablePasswordProperty, "labelPassword");
   }

   private void _accessWindowPass_OnEnterValidPassword(object sender, EventArgs e)
   {
       _isAccessChecked = true;
       Local.Common.AccessARM = TypeAccessARM.Admin;
       ChangeVisibilityLocalProperties(true);
       ChangeVisibilityGlobalProperties(true);
       ChangeVisibilityLocalPropertyByIsVisibleProperty();
       _accessWindowPass.Hide();
       OnPasswordChecked(this, true);
   }

   private void _accessWindowPass_OnClosePasswordBox(object sender, EventArgs e)
   {
       Local.Common.AccessARM = TypeAccessARM.Operator;
   }

   private void _accessWindowPass_OnEnterInvalidPassword(object sender, EventArgs e)
   {
       Local.Common.AccessARM = TypeAccessARM.Operator;
       _accessWindowPass.Hide();

       OnPasswordChecked(this, false);
   }

   private void CheckLocalAccess()
   {
       if (!_isAccessChecked)
       {
           Task.Run(() =>
              Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
              {
                  Local.Common.AccessARM = TypeAccessARM.Operator;
                  _accessWindowPass.ShowDialog();
              }));
       }
       else
           _isAccessChecked = false;
   }

   public void SetPassword(string password)
   {
       _accessWindowPass.ValidPassword = password;
   }

   #endregion 

        #region Handler

   private void Common_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
    {
       switch (e.PropertyName)
       {
           case nameof(CommonLocal.Language):
               SetResourcelanguage();
               ChangeLanguage();
               OnLanguageChange(this, Local.Common.Language);

               break;

                case nameof(CommonLocal.AccessARM):
               switch (Local.Common.AccessARM)
               {
                   case TypeAccessARM.Admin:
                            ChangeReadOnlyStatusGlobalProperties(true);
                            ChangeVisibilityGlobalProperties(true);
                            ChangeVisibilityLocalProperties(true);                     
                            CheckLocalAccess();
                            break;
                   case TypeAccessARM.Commander:
                            ChangeReadOnlyStatusGlobalProperties(true);
                            ChangeVisibilityGlobalProperties(false);
                            ChangeVisibilityLocalProperties(false);
                            break;
                   case TypeAccessARM.Operator:
                            ChangeReadOnlyStatusGlobalProperties(true);
                            ChangeVisibilityGlobalProperties(false);
                            ChangeVisibilityLocalProperties(false);
                       break;
               }
               ChangeVisibilityLocalPropertyByIsVisibleProperty();
               break;          
            default:
               break;
       }
    }

        private void ChangeVisibilityLocalProperties(bool browsable)
        {
            PropertyLocal.Properties[nameof(Local.ARD3)].IsBrowsable = browsable;
            PropertyLocal.Properties[nameof(Local.ARD2)].IsBrowsable = browsable;
            PropertyLocal.Properties[nameof(Local.ARD1)].IsBrowsable = browsable;
            PropertyLocal.Properties[nameof(Local.Tuman)].IsBrowsable = browsable;
            PropertyLocal.Properties[nameof(Local.CmpPA)].IsBrowsable = browsable;
            PropertyLocal.Properties[nameof(Local.CmpRR)].IsBrowsable = browsable;
            PropertyLocal.Properties[nameof(Local.ADSB)].IsBrowsable = browsable;
            PropertyLocal.Properties[nameof(Local.DbServer)].IsBrowsable = browsable;
            PropertyLocal.Properties[nameof(Local.DspServer)].IsBrowsable = browsable;
            PropertyLocal.Properties[nameof(Local.EdServer)].IsBrowsable = browsable;
            PropertyLocal.Properties[nameof(Local.VoiceInterference)].IsBrowsable = browsable;
            PropertyLocal.Properties[nameof(Local.Spoofing)].IsBrowsable = browsable;
            PropertyLocal.Properties[nameof(Local.Cicada)].IsBrowsable = browsable;
            PropertyLocal.Properties[nameof(Local.UAVProperties)].IsBrowsable = browsable;
        }

        private void ChangeVisibilityGlobalProperties(bool browsable)
        {
            PropertyGlobal.Properties[nameof(Global.RangeJamming)].IsBrowsable = browsable;
            PropertyGlobal.Properties[nameof(Global.RangeRadioRecon)].IsBrowsable = browsable;
            PropertyGlobal.Properties[nameof(Global.Amplifiers)].IsBrowsable = browsable;
            PropertyGlobal.Properties[nameof(Global.GnssInaccuracy)].IsBrowsable = browsable;
            PropertyGlobal.Properties[nameof(Global.NumberAveragingBearing)].IsBrowsable = browsable;
            PropertyGlobal.Properties[nameof(Global.NumberAveragingPhase)].IsBrowsable = browsable;
            PropertyGlobal.Properties[nameof(Global.NumberChannels)].IsBrowsable = browsable;
            PropertyGlobal.Properties[nameof(Global.NumberIri)].IsBrowsable = browsable;
            PropertyGlobal.Properties[nameof(Global.Priority)].IsBrowsable = browsable;
            PropertyGlobal.Properties[nameof(Global.OperationTime)].IsBrowsable = browsable;
            PropertyGlobal.Properties[nameof(Global.Threshold)].IsBrowsable = browsable;
            PropertyGlobal.Properties[nameof(Global.TimeSearch)].IsBrowsable = browsable;
            PropertyGlobal.Properties[nameof(Global.PingIntervalFHSAP)].IsBrowsable = browsable;
            PropertyGlobal.Properties[nameof(Global.DetectionAccuracyUAV)].IsBrowsable = browsable;
            PropertyGlobal.Properties[nameof(Global.TimeRadiateUAV)].IsBrowsable = browsable;
        }

        private void ChangeReadOnlyStatusGlobalProperties(bool browsable)
        {
            PropertyGlobal.Properties[nameof(Global.GnssInaccuracy)].IsReadOnly = browsable;
        }

        private void ChangeVisibilityLocalPropertyByIsVisibleProperty()
        {
            if (Local.Common.AccessARM == TypeAccessARM.Admin)
            {
                PropertyLocal.Properties[nameof(Local.ARD1)].IsBrowsable = Local.ARD1.IsVisible;
                PropertyLocal.Properties[nameof(Local.ARD2)].IsBrowsable = Local.ARD2.IsVisible;
                PropertyLocal.Properties[nameof(Local.ARD3)].IsBrowsable = Local.ARD3.IsVisible;
                PropertyLocal.Properties[nameof(Local.Spoofing)].IsBrowsable = Local.Spoofing.IsVisible;
                PropertyLocal.Properties[nameof(Local.Cicada)].IsBrowsable = Local.Cicada.IsVisible;
                PropertyLocal.Properties[nameof(Local.VoiceInterference)].IsBrowsable = Local.VoiceInterference.IsVisible;
                PropertyLocal.Properties[nameof(Local.Tuman)].IsBrowsable = Local.Tuman.IsVisible;
                PropertyLocal.Properties[nameof(Local.CmpPA)].IsBrowsable = Local.CmpPA.IsVisible;
                PropertyLocal.Properties[nameof(Local.CmpRR)].IsBrowsable = Local.CmpRR.IsVisible;
                PropertyLocal.Properties[nameof(Local.EdServer)].IsBrowsable = Local.EdServer.IsVisible;
            }

            PropertyLocal.Properties[nameof(Local.ARONE1)].IsBrowsable = Local.ARONE1.IsVisible;
            PropertyLocal.Properties[nameof(Local.ARONE2)].IsBrowsable = Local.ARONE2.IsVisible;
            PropertyLocal.Properties[nameof(Local.AP_2Properties)].IsBrowsable = Local.AP_2Properties.IsVisible;
        }

        private void Global_PropertyChanged1(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
           if (e.PropertyName == nameof(Global.SignHeadingAngle))
           {
               OnSignHeadingAngle(this, Global.SignHeadingAngle);
               return;
           }
        }

        private void PCProperties_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
         //   OnUpdateLocalProperties(this, Local);
            switch (e.PropertyName)
            {
                case nameof(PCProperties.Type):
                    if (Local.PCProperties.Type == TypePC.Berezina)
                    {
                        Local.PCProperties.TypeOfConection = TypeOfConection.Tainet;
                    }
                    else if (Local.PCProperties.Type == TypePC.Garant)
                    { 
                        Local.PCProperties.TypeOfConection = TypeOfConection.APD; 
                    }
                    else if (Local.PCProperties.Type == TypePC.Ross)
                    {
                        Local.PCProperties.TypeOfConection = TypeOfConection.RadioModem;
                    }
                    break;
                case nameof(PCProperties.IsBerezinaVisible):
                    if (Local.PCProperties.IsGarantVisible == false && Local.PCProperties.IsBerezinaVisible == false)
                    {
                        PropertyLocal.Properties[nameof(Local.PCProperties)].IsBrowsable = false;
                        Local.PCProperties.State = false;
                    }
                    else
                    {
                        PropertyLocal.Properties[nameof(Local.PCProperties)].IsBrowsable = true;
                        Local.PCProperties.State = true;
                    }
                    break;
                case nameof(PCProperties.IsGarantVisible):
                    if (Local.PCProperties.IsGarantVisible == false && Local.PCProperties.IsBerezinaVisible == false)
                    {
                        PropertyLocal.Properties[nameof(Local.PCProperties)].IsBrowsable = false;
                        Local.PCProperties.State = false;
                    }
                    else
                    {
                        PropertyLocal.Properties[nameof(Local.PCProperties)].IsBrowsable = true;
                        Local.PCProperties.State = true;
                    }
                    break;
                case nameof(PCProperties.IsRossVisible):
                    if (Local.PCProperties.IsRossVisible == false && Local.PCProperties.IsRossVisible == false)
                    {
                        PropertyLocal.Properties[nameof(Local.PCProperties)].IsBrowsable = false;
                        Local.PCProperties.State = false;
                    }
                    else
                    {
                        PropertyLocal.Properties[nameof(Local.PCProperties)].IsBrowsable = true;
                        Local.PCProperties.State = true;
                    }
                    break;
                default:
                    break;
            }
       
        }

   #endregion

        #region Init

   private void InitEvent()
    {
       Local.Common.PropertyChanged += Common_PropertyChanged;
       Local.PCProperties.PropertyChanged += PCProperties_PropertyChanged;
       Local.PropertyChanged += Local_PropertyChanged;
       oldGlobal.PropertyChanged += Global_PropertyChanged1;
       ((GlobalProperties)Resources["globalProperties"]).PropertyChanged += Global_PropertyChanged;
    }

        private void Local_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            ChangeVisibilityLocalPropertyByIsVisibleProperty();
        }

        private void Global_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
           if (e.PropertyName == nameof(GlobalProperties.TypeRadioRecon) || e.PropertyName == nameof(GlobalProperties.DetectionFHSS) || e.PropertyName == nameof(GlobalProperties.TypeRadioSuppr))
           {
               if (!oldGlobal.Compare(Global))
               {
                   oldGlobal.Update(Global.Clone());
                   OnUpdateGlobalProperties(this, Global);
               }
           }

            if (e.PropertyName == nameof(Global.SpoofingVisibility))
            {

                PropertyGlobal.Properties[nameof(Global.Latitude)].IsBrowsable = Global.SpoofingVisibility;
                PropertyGlobal.Properties[nameof(Global.Longitude)].IsBrowsable = Global.SpoofingVisibility;
                return;
            }
        }

   private void InitEditors()
   {
       PropertyLocal.Editors.Add(new EditorLocal(nameof(Local.CmpRR), Local.GetType(), Local.CmpRR.GetType()));
       PropertyLocal.Editors.Add(new EditorLocal(nameof(Local.CmpPA), Local.GetType(), Local.CmpPA.GetType()));
       PropertyLocal.Editors.Add(new EditorLocal(nameof(Local.ARONE1), Local.GetType(), Local.ARONE1.GetType()));
       PropertyLocal.Editors.Add(new EditorLocal(nameof(Local.ARONE2), Local.GetType(), Local.ARONE2.GetType()));
       PropertyLocal.Editors.Add(new EditorLocal(nameof(Local.Tuman), Local.GetType(), Local.Tuman.GetType()));

       PropertyLocal.Editors.Add(new EditorLocal(nameof(Local.ARD1), Local.GetType(), Local.ARD1.GetType()));
       PropertyLocal.Editors.Add(new EditorLocal(nameof(Local.ARD2), Local.GetType(), Local.ARD2.GetType()));
       PropertyLocal.Editors.Add(new EditorLocal(nameof(Local.ARD3), Local.GetType(), Local.ARD3.GetType()));

       PropertyLocal.Editors.Add(new EditorLocal(nameof(Local.DspServer), Local.GetType(), Local.DspServer.GetType()));
       PropertyLocal.Editors.Add(new EditorLocal(nameof(Local.DbServer), Local.GetType(), Local.DbServer.GetType()));
       PropertyLocal.Editors.Add(new EditorLocal(nameof(Local.EdServer), Local.GetType(), Local.EdServer.GetType()));
       PropertyLocal.Editors.Add(new EditorLocal(nameof(Local.PCProperties), Local.GetType(), Local.PCProperties.GetType()));
       PropertyLocal.Editors.Add(new EditorLocal(nameof(Local.AP_2Properties), Local.GetType(), Local.AP_2Properties.GetType()));
       PropertyLocal.Editors.Add(new EditorLocal(nameof(Local.ADSB), Local.GetType(), Local.ADSB.GetType()));
       PropertyLocal.Editors.Add(new EditorLocal(nameof(Local.Common), Local.GetType(), Local.Common.GetType()));
       PropertyLocal.Editors.Add(new EditorLocal(nameof(Local.CoordinatesProperty), Local.GetType(), Local.CoordinatesProperty.GetType()));
       PropertyLocal.Editors.Add(new EditorLocal(nameof(Local.VoiceInterference), Local.GetType(), Local.VoiceInterference.GetType()));
       PropertyLocal.Editors.Add(new EditorLocal(nameof(Local.Spoofing), Local.GetType(), Local.Spoofing.GetType()));

       PropertyLocal.Editors.Add(new EditorLocal(nameof(Local.Cicada), Local.GetType(), Local.Cicada.GetType()));
       PropertyLocal.Editors.Add(new EditorLocal(nameof(Local.UAVProperties), Local.GetType(), Local.UAVProperties.GetType()));

       PropertyGlobal.Editors.Add(new EditorGlobal(nameof(Global.FFTResolution), Global.GetType()));
       PropertyGlobal.Editors.Add(new EditorGlobal(nameof(Global.Amplifiers), Global.GetType()));
       PropertyGlobal.Editors.Add(new EditorGlobal(nameof(Global.TypeRadioRecon), Global.GetType()));
       PropertyGlobal.Editors.Add(new EditorGlobal(nameof(Global.HeadingAngle), Global.GetType()));
   }

   private void InitProperties()
   {
       oldGlobal = Global.Clone();
       foreach (var property in PropertyGlobal.Properties)
       {
           property.PropertyValue.PropertyValueException += GlobalPropertyException;
       }

       oldLocal = Local.Clone();
       foreach (var property in PropertyLocal.Properties)
       {
           if (property.PropertyValue.SubProperties != null)
           {
               foreach (var subProperty in property.PropertyValue.SubProperties)
                   subProperty.PropertyValue.PropertyValueException += LocalPropertyException;
               continue;
           }
           property.PropertyValue.PropertyValueException += LocalPropertyException;
       }
   }

   #endregion

        #region Language

   Dictionary<string, string> TranslateDic;

   private void SetResourcelanguage()
   {
       ResourceDictionary dict = new ResourceDictionary();
       try
       {
           switch (Local.Common.Language)
           {
               case Languages.Eng:
                   dict.Source = new Uri("/ControlProperties;component/Languages/StringResource.EN.xaml",
                                 UriKind.Relative);
                   break;
               case Languages.Rus:
                   dict.Source = new Uri("/ControlProperties;component/Languages/StringResource.xaml",
                                      UriKind.Relative);
                   break;
               case Languages.Azr:
                   dict.Source = new Uri("/ControlProperties;component/Languages/StringResource.AZ.xaml",
                                           UriKind.Relative);
                   break;
                    default:
                   dict.Source = new Uri("/ControlProperties;component/Languages/StringResource.xaml",
                                     UriKind.Relative);
                   break;
           }

           this.Resources.MergedDictionaries.Add(dict);
       }
       catch (Exception )
       { }
   }


   private void SetNameCategoryGlobal(string nameCategory)
   {
       try
       {
           PropertyGlobal.Categories.First(t => t.Name == nameCategory).HeaderCategoryName = TranslateDic[nameCategory];
       }
       catch (Exception)
       {

       }
   }

   private void SetNameCategoryLocal(string nameCategory)
   {
       try
       {
           PropertyLocal.Categories.First(t => t.Name == nameCategory).HeaderCategoryName = TranslateDic[nameCategory];
       }
       catch (Exception)
       {

       }
   }

   private void SetNamePropery(string nameProperty)
   {
       try
       {
           PropertyGlobal.Properties[nameProperty].DisplayName = TranslateDic[nameProperty];

           if (TranslateDic.ContainsKey(nameProperty + "ToolTip"))
               PropertyGlobal.Properties[nameProperty].ToolTip = TranslateDic[nameProperty + "ToolTip"];
           else
               PropertyGlobal.Properties[nameProperty].ToolTip = TranslateDic[nameProperty];

       }
       catch (Exception)
       {

       }
   }

   private void ChangeLanguage()
   {
       LoadDictionary();

       foreach (var prop in PropertyGlobal.Properties)
       {
           SetNamePropery(prop.Name);
       }

       foreach(var category in PropertyGlobal.Categories)
       {
           SetNameCategoryGlobal(category.Name);
       }

       foreach (var categoryLocal in PropertyLocal.Categories)
       {
           SetNameCategoryLocal(categoryLocal.Name);
       }

       switch (Local.Common.Language)
       {
           case Languages.Rus:
               {
                   butDefault.ToolTip = "Стандартные настройки";
                   lDefault.Content = "C";
               }
               break;
           case Languages.Eng:
               {
                   butDefault.ToolTip = "Default settings";
                   lDefault.Content = "D";
               }
               break;
           case Languages.Azr:
                    {
                        butDefault.ToolTip = "Default settings";
                        lDefault.Content = "D";
                    }
                    break;
                default:
               break;
       }
   }

   void LoadDictionary()
   {
       XmlDocument xDoc = new XmlDocument();
       if (System.IO.File.Exists(System.IO.Directory.GetCurrentDirectory() + "\\Languages" + "\\TranslationControlProperties.xml"))
           xDoc.Load(System.IO.Directory.GetCurrentDirectory() + "\\Languages" + "\\TranslationControlProperties.xml");
       else
       {
           switch (Local.Common.Language)
           {

               case Languages.Rus:
                  // MessageBox.Show("Файл XMLTranslation.xml не найден!", "Ошибка!");
                   break;
               case Languages.Eng:
                   //MessageBox.Show("File XMLTranslation.xml not found!", "Error!");
                   break;
               case Languages.Azr:
                   //MessageBox.Show("File XMLTranslation.xml not found!", "Error!");
                   break;
                    default:
                  // MessageBox.Show("Файл XMLTranslation.xml не найден!", "Ошибка!");
                   break;
           }

           return;
       }
       TranslateDic = new Dictionary<string, string>();
       // получим корневой элемент
       XmlElement xRoot = xDoc.DocumentElement;
       foreach (XmlNode x2Node in xRoot.ChildNodes)
       {
           if (x2Node.NodeType == XmlNodeType.Comment)
               continue;

           // получаем атрибут ID
           if (x2Node.Attributes.Count > 0)
           {
               XmlNode attr = x2Node.Attributes.GetNamedItem("ID");
               if (attr != null)
               {
                   foreach (XmlNode childnode in x2Node.ChildNodes)
                   {
                       // если узел - language
                       if (childnode.Name == Local.Common.Language.ToString())
                       {
                           if (!TranslateDic.ContainsKey(attr.Value))
                               TranslateDic.Add(attr.Value, childnode.InnerText);
                       }

                   }
               }
           }
       }
   }

   #endregion

        #region Properties exceptions

   private void LocalPropertyException(object sender, ValueExceptionEventArgs e)
   {
       PropertyLocal.Properties[(sender as PropertyItemValue).ParentProperty.ParentValue.ParentProperty.Name].SetValue((sender as PropertyItemValue).Value);
   }

   private void GlobalPropertyException(object sender, ValueExceptionEventArgs e)
   {
       PropertyGlobal.Properties[(sender as PropertyItemValue).ParentProperty.Name].SetValue((sender as PropertyItemValue).Value);
   }

   #endregion

        #region Click

   private void ApplyClick(object sender, RoutedEventArgs e)
   {
       if (tabGlobal.IsSelected)
       {
           string errorGlobal = Valid(Global);

           if (errorGlobal != "")
           {
               MessageBox.Show(errorGlobal, $"{TranslateDic["Error"]}! {TranslateDic[nameof(Global)]} {TranslateDic["Properties"]}!", MessageBoxButton.OK, MessageBoxImage.Warning);
           }
           else if (!oldGlobal.Compare(Global))
           {
               oldGlobal.Update(Global.Clone());
               OnUpdateGlobalProperties(this, Global);
           }
       }

       if (tabLocal.IsSelected)
       {
           var errorLocal = ValidLocal();

           if (errorLocal != "")
           {
               MessageBox.Show(errorLocal, $"{TranslateDic["Error"]}! {TranslateDic[nameof(Local)]} {TranslateDic["Properties"]}!", MessageBoxButton.OK, MessageBoxImage.Warning);
           }
           else if (!oldLocal.Compare(Local))
           {
               oldLocal = Local.Clone();

               OnUpdateLocalProperties(this, Local);

           }
       }
   }

   public void PerformApply()
   {
       //ApplyClick(this, null);

       if (tabGlobal.IsSelected)
       {
           string errorGlobal = Valid(Global);

           if (errorGlobal != "")
           {
               MessageBox.Show(errorGlobal, $"{TranslateDic["Error"]}! {TranslateDic[nameof(Global)]} {TranslateDic["Properties"]}!", MessageBoxButton.OK, MessageBoxImage.Warning);
           }
           else
           {
               oldGlobal.Update(Global.Clone());
               OnUpdateGlobalProperties(this, Global);
           }
       }

       if (tabLocal.IsSelected)
       {
           var errorLocal = ValidLocal();

           if (errorLocal != "")
           {
               MessageBox.Show(errorLocal, $"{TranslateDic["Error"]}! {TranslateDic[nameof(Local)]} {TranslateDic["Properties"]}!", MessageBoxButton.OK, MessageBoxImage.Warning);
           }
           else
           {
               oldLocal = Local.Clone();

               OnUpdateLocalProperties(this, Local);
           }
       }
   }

   private string ValidLocal()
   {
       string Error = "";

       var strValid = Valid(Local.ARONE1);
       if (strValid != "")
           Error += $"{TranslateDic[nameof(Local.ARONE1)]}: {strValid}";

       strValid = Valid(Local.ARONE2);
       if (strValid != "")
           Error += $"{TranslateDic[nameof(Local.ARONE2)]}: {strValid}";

       strValid = Valid(Local.ADSB);
       if (strValid != "")
           Error += $"{TranslateDic[nameof(Local.ADSB)]}: {strValid}";

       strValid = Valid(Local.DbServer);
       if (strValid != "")
           Error += $"{TranslateDic[nameof(Local.DbServer)]}: {strValid}";

       strValid = Valid(Local.DspServer);
       if (strValid != "")
           Error += $"{TranslateDic[nameof(Local.DspServer)]}: {strValid}";

       strValid = Valid(Local.ARD1);
       if (strValid != "")
           Error += $"{TranslateDic[nameof(Local.ARD1)]}: {strValid}";

       strValid = Valid(Local.ARD2);
       if (strValid != "")
           Error += $"{TranslateDic[nameof(Local.ARD2)]}: {strValid}";

       strValid = Valid(Local.ARD3);
       if (strValid != "")
           Error += $"{TranslateDic[nameof(Local.ARD3)]}: {strValid}";

       strValid = Valid(Local.Spoofing);
       if (strValid != "")
           Error += $"{TranslateDic[nameof(Local.Spoofing)]}: {strValid}";

            strValid = Valid(Local.Cicada);
            if (strValid != "")
                Error += $"{TranslateDic[nameof(Local.Cicada)]}: {strValid}";

            strValid = Valid(Local.PCProperties);
       if (strValid != "")
           Error += $"{TranslateDic[nameof(Local.PCProperties)]}: {strValid}";

       strValid = Valid(Local.AP_2Properties);
       if (strValid != "")
           Error += $"{TranslateDic[nameof(Local.AP_2Properties)]}: {strValid}";

            strValid = Valid(Local.VoiceInterference);
       if (strValid != "")
            Error += $"{TranslateDic[nameof(Local.VoiceInterference)]}: {strValid}";
           
            return Error;

   }

   private string Valid(object instance)
   {
       string Error = "";
       List<System.ComponentModel.DataAnnotations.ValidationResult> results = new List<System.ComponentModel.DataAnnotations.ValidationResult>();
       ValidationContext context = new ValidationContext(instance);
       if (!Validator.TryValidateObject(instance, context, results, true))
       {
           foreach (var error in results)
           {
               if (TranslateDic.ContainsKey(error.MemberNames.First()))
                   Error += TranslateDic[error.MemberNames.First()] + "\n";
           }
           return Error;
       }
       return Error;
   }

   private void NotApplyClick(object sender, RoutedEventArgs e)
   {
       if (!Global.Compare(oldGlobal))
           Global.Update(oldGlobal);

       if (!Local.Compare(oldLocal))
           Local.Update(oldLocal);
   }

   public enum PropertiesType : byte
   {
       Global,
       Local
   }

   public delegate void DefaultEventHandler(object sender, PropertiesType propertiesType);
   public event DefaultEventHandler DefaultEvent;

   private void DefaultClick(object sender, RoutedEventArgs e)
   {
       if (tabGlobal.IsSelected)
       {
           //Global.DetectionFHSS = 0;
           //Global.TypeRadioRecon = EnumTypeRadioRecon.WithoutBearing;
           //Global.TypeRadioSuppr = EnumTypeRadioSuppr.FWS;

           //Global.PingIntervalFHSAP = 5000;

           //Global.NumberAveragingPhase = 3;
           //Global.NumberAveragingBearing = 3;
           //Global.HeadingAngle = 0;
           //Global.SignHeadingAngle = true;

           //Global.NumberChannels = 4;
           //Global.NumberIri = 10;
           //Global.OperationTime = 15;
           //Global.Priority = 1;
           //Global.Threshold = -80;
           //Global.TimeRadiateFWS = 5000;

           //Global.SectorSearch = 10;
           //Global.TimeSearch = 300;

           //Global.FFTResolution = 1;
           //Global.TimeRadiateFHSS = 900;

           //ApplyClick(this, null);

           DefaultEvent?.Invoke(this, PropertiesType.Global);
       }
       if (tabLocal.IsSelected)
       {
           //Local.DspServer.Port = 10001;
           //Local.DbServer.Port = 8302;
           //Local.EdServer.Port = 10009;
           //Local.ADSB.Port = 30005;
           //Local.ARD1.Address = 1;
           //Local.ARD2.Address = 1;
           //Local.ARD3.Address = 1;

           DefaultEvent?.Invoke(this, PropertiesType.Local);
       }
   }

   #endregion

        #region Update ComPorts

   private bool needUpdate = true;

   private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
   {
       if ((sender as TabControl).SelectedIndex == 1 && needUpdate)
       {
           needUpdate = false;
           (Resources["PortsNames"] as ComPortsNames).UpdatePort();
       }
       else if ((sender as TabControl).SelectedIndex != 1)
           needUpdate = true;
   }

        #endregion

        #region Validation

        private void Validate()
        {
            bool isvalid = true;
            string error = "";
            Action<object> funcValidation = (object obj) =>
            {
                foreach (var property in obj.GetType().GetProperties())
                {
                    foreach (var subProperty in property.PropertyType.GetProperties())
                    {
                        try
                        {
                            if (!(property.Name == "DependencyObjectType" || property.Name == "Dispatcher"))
                            {
                                if (!string.IsNullOrWhiteSpace(Convert.ToString(((IDataErrorInfo)property.GetValue(obj))[subProperty.Name])))
                                {
                                    error += $"{TranslateDic[property.Name]}: {TranslateDic[subProperty.Name]}" + "\n";
                                    isvalid = false;
                                }
                            }
                        }
                        catch
                        {
                            continue;
                        }
                    }
                }
                IsValid = isvalid;
                ShowToolTip = !isvalid;
                ErrorMessage = error;
                return;
            };

            if (IsLocalSelected)
            {
                funcValidation(Local);
            }
            else
            {
                funcValidation(Global);
            }
        }

        private void _this_Loaded(object sender, RoutedEventArgs e)
        {
            ChangeReadOnlyStatusGlobalProperties(true);
            ChangeVisibilityGlobalProperties(false);
            ChangeVisibilityLocalProperties(false);
            ChangeVisibilityLocalPropertyByIsVisibleProperty();
        }

        private void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            Validate();
        }
        #endregion

    }
}
