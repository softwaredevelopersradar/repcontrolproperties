﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControlProperties
{
   public class Level : ObservableCollection<byte>
    {
        public Level()
        {
            Add(0);
            Add(1);
        }
    }
}
