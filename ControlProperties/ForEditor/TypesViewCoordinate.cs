﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
namespace ControlProperties
{
    public class TypesViewCoordinate : ObservableCollection<string>
    {
        public TypesViewCoordinate()
        {
            Add("DD.dddddd");
            Add("DD MM.mmmm");
            Add("DD MM SS.ss");
        }

        public void UpdateNames(List<string> updatedViews)
        {
            if (Count > updatedViews.Count) return;
            for (int i = 0; i < Count; i++)
                this[i] = updatedViews[i];
            if (Count < updatedViews.Count)
                for (int i = Count; i < updatedViews.Count; i++)
                    Add(updatedViews[i]);
        }

        public void SetNewNames(List<string> newViews)
        {
            Clear();
            foreach (var system in newViews)
                Add(system);
        }
    }
}
