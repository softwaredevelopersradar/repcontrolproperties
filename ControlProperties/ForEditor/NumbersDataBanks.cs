﻿using System.Collections.ObjectModel;
namespace ControlProperties
{
    public class NumbersDataBanks: ObservableCollection<int>
    {
        private int firstNumber = 0;
        private int lastNumber = 9;

        public NumbersDataBanks()
        {
            for (int i = firstNumber; i <= lastNumber; i++)
                Add(i);
        }
    }
}
