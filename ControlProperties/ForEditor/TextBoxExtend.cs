﻿using System.Windows.Input;
using System.Windows.Data;
using System.Windows;
using System.Windows.Controls;
using System;

namespace ControlProperties
{
    public static class TextBoxExtend
    {
        #region OnTyping

        public static readonly DependencyProperty CommitOnIntTypingProperty = DependencyProperty.RegisterAttached("CommitOnIntTyping", typeof(bool), typeof(TextBoxExtend), new FrameworkPropertyMetadata(false, OnCommitOnIntTypingChanged));

        private static void OnCommitOnIntTypingChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var textbox = sender as TextBox;
            if (textbox == null) return;

            var wasBound = (bool)(e.OldValue);
            var needToBind = (bool)(e.NewValue);

            if (wasBound)
            {
                textbox.KeyUp -= TextBoxCommitValueWhileTyping;
                textbox.PreviewTextInput -= TextboxCommitPreviewTextInput;
                textbox.PreviewKeyDown -= Textbox_PreviewKeyDown;
            }

            if (needToBind)
            {
                textbox.PreviewTextInput += TextboxCommitPreviewTextInput;
                textbox.KeyUp += TextBoxCommitValueWhileTyping;
                textbox.PreviewKeyDown += Textbox_PreviewKeyDown;
            }
        }


        private static void TextboxCommitPreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = "-0123456789".IndexOf(e.Text) < 0;
        }

        static void TextBoxCommitValueWhileTyping(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.OemMinus ||  e.Key == Key.Escape) //e.Key == Key.Back ||
                return;

            var textbox = sender as TextBox;

            if (textbox == null) return;

            if (textbox.Text == "") return;

            if (e.Key == Key.Back && textbox.Text == "-")
                return;

            BindingExpression expression = textbox.GetBindingExpression(TextBox.TextProperty);
            if (expression != null) expression.UpdateSource();
            e.Handled = true;
        }

        public static void SetCommitOnIntTyping(TextBox target, bool value)
        {
            target.SetValue(CommitOnIntTypingProperty, value);
        }

        public static bool GetCommitOnIntTyping(TextBox target)
        {
            return (bool)target.GetValue(CommitOnIntTypingProperty);
        }
        #endregion

        #region CommitOnIP
        public static readonly DependencyProperty CommitOnIPProperty = DependencyProperty.RegisterAttached("CommitOnIP",
            typeof(bool), typeof(TextBoxExtend), new FrameworkPropertyMetadata(false, OnCommitOnIPChanged));

        private static void OnCommitOnIPChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var textbox = sender as TextBox;
            if (textbox == null) return;

            var wasBound = (bool)(e.OldValue);
            var needToBind = (bool)(e.NewValue);

            if (wasBound)
            {
                textbox.KeyUp -= TextBoxCommitOnIpValue;
                textbox.PreviewTextInput -= TextboxCommitOnIpPreviewInput;
                //textbox.PreviewKeyDown -= Textbox_PreviewKeyDown;
            }

            if (needToBind)
            {
                textbox.PreviewTextInput += TextboxCommitOnIpPreviewInput;
                textbox.KeyUp += TextBoxCommitOnIpValue;
                //textbox.PreviewKeyDown += Textbox_PreviewKeyDown;
            }
        }

        private static void Textbox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Tab)
            {
                var textbox = sender as TextBox;
                var parent = textbox.Parent;
                foreach (var final in (parent as Grid).Children)
                {
                    if (final.Equals(sender)) continue;

                    if (!(final is TextBox)) continue;
                    if ((final as TextBox).IsEnabled && (final as TextBox).Focusable && !(final as TextBox).IsReadOnly)
                    {
                        (final as UIElement).Focus();
                        e.Handled = true;
                        return;
                    }
                }
                (sender as UIElement).Focus();
                e.Handled = true;
            }

        }

        private static void TextboxCommitOnIpPreviewInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = "0123456789.".IndexOf(e.Text) < 0;
        }

        static void TextBoxCommitOnIpValue(object sender, KeyEventArgs e)
        {
            if ( e.Key == Key.Escape || e.Key == Key.Tab) //e.Key == Key.Back ||
                return;

            var textbox = sender as TextBox;

            if (textbox == null) return;

            if (textbox.Text == "") return;

            BindingExpression expression = textbox.GetBindingExpression(TextBox.TextProperty);
            if (expression != null) expression.UpdateSource();
            e.Handled = true;
        }

        public static void SetCommitOnIP(TextBox target, bool value)
        {
            target.SetValue(CommitOnIPProperty, value);
        }

        public static bool GetCommitOnIP(TextBox target)
        {
            return (bool)target.GetValue(CommitOnIPProperty);
        }
        #endregion}
    }
}