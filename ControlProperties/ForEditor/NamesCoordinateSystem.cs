﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using ModelsTablesDBLib;

namespace ControlProperties
{
    public class NamesCoordinateSystem : ObservableCollection<string>
    {
        public NamesCoordinateSystem()
        {
            foreach (var system in Enum.GetNames(typeof(EnumCoordinateSystem)))
                Add(system);
        }

        public void UpdateNames(List<string> updatedNames)
        {
            if (Count > updatedNames.Count) return;
            for (int i = 0; i < Count; i++)
                this[i] = updatedNames[i];
            if (Count < updatedNames.Count)
                for (int i = Count; i < updatedNames.Count; i++)
                    Add(updatedNames[i]);
        }

        public void SetNewNames(List<string> newNames)
        {
            Clear();
            foreach (var system in newNames)
                Add(system);            
        }
    }
}
