﻿using System.Collections.ObjectModel;

namespace ControlProperties
{
    public class NumbersARM: ObservableCollection<byte>
    {
        public NumbersARM()
        {
            Add(1);
            Add(2);
            Add(3);
        }
    }
}
