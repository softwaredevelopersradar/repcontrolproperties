﻿using ModelsTablesDBLib;
using System;

namespace ControlProperties.Extensions
{
    public static class CoordExtension
    {
        public static string ToStringDirection(this Coord coord)
        {
            if ((coord.Latitude == -1 && coord.Longitude == -1 && coord.Altitude == -1) || (coord.Latitude == -1 || coord.Longitude == -1))
                return " ";

            if (coord.Altitude < 0)
                return $"{Math.Abs(coord.Latitude)} {(coord.Latitude < 0 ? ("S") : ("N"))} : {Math.Abs(coord.Longitude)} {(coord.Longitude < 0 ? ("E") : ("W"))} : — ";
            return $"{Math.Abs(coord.Latitude)} {(coord.Latitude < 0 ? ("S") : ("N"))} : {Math.Abs(coord.Longitude)} {(coord.Longitude < 0 ? ("E") : ("W"))} : {Math.Abs(coord.Altitude)}";
        }
    }
}
