﻿using System;
using System.Globalization;
using System.Windows.Data;
using ModelsTablesDBLib;

namespace ControlProperties
{
    public class ConverterHeadingAngle : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (bool)value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (EnumTypeRadioRecon)System.Convert.ToByte(value);
        }
    }
}
