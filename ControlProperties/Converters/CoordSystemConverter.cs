﻿using System;
using System.Globalization;
using System.Windows.Data;
using ModelsTablesDBLib;

namespace ControlProperties
{
    public class CoordSystemConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (int)value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (EnumCoordinateSystem)value;
        }
    }
}
