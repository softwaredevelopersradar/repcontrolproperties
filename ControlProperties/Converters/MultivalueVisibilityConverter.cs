﻿using ModelsTablesDBLib;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Data;

namespace ControlProperties
{
   public class MultivalueVisibilityConverter : IMultiValueConverter
    {
        public object Convert(object[] value, Type targetType, object parameter, CultureInfo culture)
        {
            
             if ((TypeAccessARM)value[1] == TypeAccessARM.Operator)
                return Visibility.Collapsed;
             else
             {
                switch((TypePC)value[0])
                {
                    case TypePC.Garant:
                        if (((string)parameter).Equals("APD"))
                            return Visibility.Visible;
                        else return Visibility.Collapsed;
                    case TypePC.Berezina:
                        if (((string)parameter).Equals("APD") || ((string)parameter).Equals("RadioModem"))
                            return Visibility.Collapsed;
                        else return Visibility.Visible;

                    case TypePC.Ross:
                    if (((string)parameter).Equals("3G4G") || ((string)parameter).Equals("RadioModem"))
                        return Visibility.Visible;
                    else return Visibility.Collapsed;
                    default:
                    if (((string)parameter).Equals("APD"))
                        return Visibility.Visible;
                    else return Visibility.Collapsed;
                }
             }
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
