﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using ModelsTablesDBLib;

namespace ControlProperties
{
    class EnumItemVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {

            switch ((TypePC)value)
            {
                case TypePC.Garant:
                    if (((string)parameter).Equals("APD"))
                        return Visibility.Visible;
                    else return Visibility.Collapsed;
                case TypePC.Berezina:
                    if (((string)parameter).Equals("APD") || ((string)parameter).Equals("RadioModem"))
                        return Visibility.Collapsed;
                    else return Visibility.Visible;

                case TypePC.Ross:
                    if (((string)parameter).Equals("R3G4G") || ((string)parameter).Equals("RadioModem"))
                        return Visibility.Visible;
                    else return Visibility.Collapsed;
                default:
                    if (((string)parameter).Equals("APD"))
                        return Visibility.Visible;
                    else return Visibility.Collapsed;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
