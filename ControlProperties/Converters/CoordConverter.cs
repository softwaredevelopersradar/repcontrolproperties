﻿using System;
using System.Globalization;
using System.Windows.Data;
using ControlProperties.Extensions;
using ModelsTablesDBLib;

namespace ControlProperties
{
    public class CoordConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return ((Coord)value).ToStringDirection();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
