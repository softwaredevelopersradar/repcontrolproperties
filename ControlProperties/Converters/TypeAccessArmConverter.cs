﻿using ModelsTablesDBLib;
using System;
using System.Globalization;
using System.Windows.Data;

namespace ControlProperties
{
    public class TypeAccessArmConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (byte)value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (TypeAccessARM)System.Convert.ToByte(value);
        }
    }
}
