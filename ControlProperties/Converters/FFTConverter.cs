﻿using System;
using System.Globalization;
using System.Windows.Data;
using ModelsTablesDBLib;

namespace ControlProperties
{
    public class FFTConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (EnumFFTResolution)value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (byte)value;
        }
    }
}
