﻿using System;
using System.Globalization;
using System.Windows.Controls;
using System.Windows.Data;

namespace ControlProperties
{
    public class ConvertCountLetters : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is string)
                return (value as string).Length + 10;
            return 0;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
