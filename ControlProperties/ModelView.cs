﻿using ModelsTablesDBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ControlProperties
{
    public class ModelView: DependencyObject
    {

        #region Private

        private GlobalProperties oldGlobal;

        private LocalProperties oldLocal;

        #endregion

        #region Properties

        public GlobalProperties Global
        {
            get { return oldGlobal; }
            set
            {
                if (oldGlobal == value) return;
                oldGlobal = value;
            }
        }

        public LocalProperties Local
        {
            get => oldLocal;
            set
            {
                if (oldLocal == value) return;
                 oldLocal = value;
            }
        }

        #endregion

        #region DependencyProperty

        public static DependencyProperty LocalProperty = DependencyProperty.Register(nameof(Local), typeof(LocalProperties), typeof(ModelView), new FrameworkPropertyMetadata(new LocalProperties(), new PropertyChangedCallback(LocalPropertyChanged)));
        public static DependencyProperty GlobalProperty = DependencyProperty.Register(nameof(Global), typeof(GlobalProperties), typeof(ModelView), new FrameworkPropertyMetadata(new GlobalProperties(), new PropertyChangedCallback(GlobalPropertyChanged)));

        #endregion

        #region PropertyChangedCallback

        private static void LocalPropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            ModelView model = (ModelView)sender;
            var wasBound = (LocalProperties)(e.OldValue);

            var needToBind = (LocalProperties)(e.NewValue);

            if (e.OldValue != e.NewValue && needToBind != null)
            {

            }
        }

        private static void GlobalPropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            ModelView model = (ModelView)sender;
            var wasBound = (GlobalProperties)(e.OldValue);

            var needToBind = (GlobalProperties)(e.NewValue);

            if (e.OldValue != e.NewValue && needToBind != null)
            {

            }
        }

        #endregion

        #region Commands

        private RelayCommand saveCommand;
        private RelayCommand revokeCommand;

        public RelayCommand SaveCommand
        {
            get => saveCommand;
            set
            {
                saveCommand = value;
            }
        }
        public RelayCommand RevokeCommand
        {
            get => revokeCommand;
            set
            {
                revokeCommand = value;
            }
        }

        #endregion
    }
}
