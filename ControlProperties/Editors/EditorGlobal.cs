﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls.WpfPropertyGrid;
using ModelsTablesDBLib;

namespace ControlProperties
{
    public class EditorGlobal : PropertyEditor
    {
        Dictionary<string, string> dictKeyDataTemplate = new Dictionary<string, string>()
        {
            { nameof(GlobalProperties.Amplifiers), "AmplifiersTypeditorKey" },
            { nameof(GlobalProperties.FFTResolution), "FFTEditorKey" },
            { nameof(GlobalProperties.TypeRadioRecon), "TypeRadioReconEditor"},
            {nameof(GlobalProperties.HeadingAngle), "HeadingAngleEditorKey" }
        };
        public EditorGlobal(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/ControlProperties;component/Resources/Dictionary.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            InlineTemplate = resource[dictKeyDataTemplate[PropertyName]];
        }
    }
}
