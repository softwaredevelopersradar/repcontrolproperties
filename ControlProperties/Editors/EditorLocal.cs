﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls.WpfPropertyGrid;
using ModelsTablesDBLib;

namespace ControlProperties
{
    public class EditorLocal : PropertyEditor
    {
        Dictionary<Type, string> dictKeyDataTemplate = new Dictionary<Type, string>()
        {
            { typeof(ComConnection), "ComPortEditorKey" },
            { typeof(CompasRI),"CompasRIEditorKey" },
            { typeof(ARDConnection), "ARDEditorKey"},
            { typeof(EndPointConnection), "EndPointEditorKey"},
            { typeof(CommonLocal), "CommonEditorKey"},
            { typeof(ARONEProperties), "ARONEEditorKey"},
            { typeof(CoordinatesProperty), "CoordinatePropertyEditor" },
            { typeof(ADSBConnection),"ADSBEditorKey"},
            { typeof(Spoofing), "SpoofingEditorKey"},
            { typeof(VoiceInterference), "VoiceInterferenceEditorKey"},
            { typeof(PCProperties), "PCPropertiesEditorKey"},
            { typeof(UAVProperties), "UAVPropertiesEditorKey" },
            { typeof(AP_2Properties), "AP_2PropertiesEditorKey"},
             { typeof(ODProperties), "ODPropertiesEditorKey"},
            {typeof(Cicada),  "CicadaEditorKey"}
        };

        public EditorLocal(string PropertyName, Type DeclaringType, Type typeProperty)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/ControlProperties;component/Resources/Dictionary.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            this.InlineTemplate = resource[dictKeyDataTemplate[typeProperty]];
        }

        //public override void ShowDialog(PropertyItemValue propertyValue, IInputElement commandSource)
        //{
        //    if (propertyValue == null) return;
        //    if (propertyValue.ParentProperty.IsReadOnly) return;
        //    FolderBrowserDialog ofd = new FolderBrowserDialog();

        //    if (ofd.ShowDialog() == DialogResult.OK)
        //    {
        //        propertyValue.SubProperties["PathData"].SetValue(ofd.SelectedPath);
        //    }
        //}
    }
}
