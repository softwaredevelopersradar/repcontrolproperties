﻿using System;
using System.Windows;
using ModelsTablesDBLib;

namespace Test
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            Properties.OnUpdateGlobalProperties += HandlerChangesGlobalProperty;
            Properties.OnUpdateLocalProperties += HandlerChangesLocalProperty;
            // Properties.Local.Common.IsVisibleAZ = false;

            // Properties.SetPassword("123");
        }
        public void HandlerChangesGlobalProperty(object sender, GlobalProperties arg)
        {
        }
        public void HandlerChangesLocalProperty(object sender, LocalProperties arg)
        {

        }

        private void ButtonShow_Click(object sender, RoutedEventArgs e)
        {
            Random random = new Random();
            if (Properties.Local.CoordinatesProperty.CoordGPS.Latitude < 0)
            {
                Properties.Local.CoordinatesProperty.CoordGPS = new Coord()
                { Altitude = random.Next(-1,7), Latitude = 23, Longitude = 22 };
            }
            else
            {
                Properties.Local.CoordinatesProperty.CoordGPS = new Coord()
                { Altitude = random.Next(-1, 7), Latitude = -23, Longitude = -22 };

            }
            Properties.Local.CoordinatesProperty.CompassPA = 340;
            Properties.Local.CoordinatesProperty.CompassRR = 250;
        }

        private void Properties_OnSignHeadingAngle(object sender, bool e)
        {
            lableL.Content = e.ToString();
        }

        private void Prop_OnPasswordChecked(object sender, bool e)
        {
            if (e)
            {
                //MessageBox.Show("Good job!");
            }
            else
            {
                MessageBox.Show("Invalid password!");
            }
        }

    }
}
